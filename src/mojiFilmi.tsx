import React, { useState, useEffect } from 'react';
import Bar from './components/Bar';
import './css/App.css';
import FilmKartica from './components/filmKartica';
import Grid from '@mui/material/Grid';
import { Box } from '@mui/material';


export default function MojiFilmi() {
  const [mojiFilmi, setMojiFilmi] = useState([]);

  useEffect(() => {
    const mojiFilmiString = localStorage.getItem('mojiFilmi');
    const mojiFilmiData = mojiFilmiString ? JSON.parse(mojiFilmiString) : [];
    const obrnjeniMojiFilmi = mojiFilmiData.reverse(); 
    setMojiFilmi(obrnjeniMojiFilmi);
  }, []);

  const odstraniIzLocalStorage = (filmId: any) => {
    const noviMojiFilmi = mojiFilmi.filter((mfilm: any) => mfilm.id !== filmId);
    localStorage.setItem('mojiFilmi', JSON.stringify(noviMojiFilmi));
    setMojiFilmi(noviMojiFilmi);
  };

  return (
    <>
      <Grid style={{ position: 'fixed', top: 0, width: '100%', height: '100%', paddingRight: 30, overflowY: 'auto' }}>
        <Bar />
        <Grid container spacing={2} >
          {mojiFilmi.length > 0 ? (
            mojiFilmi.map((film: any, index: any) => (
              <Grid item xs={12} sm={6} md={4} lg={2} key={index}>
                <FilmKartica film={film} odstraniIzLocalStorage={odstraniIzLocalStorage} />
              </Grid>
            ))
          ) : (
            <Box style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh', textAlign: 'center', color:"white", marginLeft:"35%" }}>
              <p>Seznam je prazen!</p>
            </Box>
          )}
        </Grid>
      </Grid>
    </>
  );
}