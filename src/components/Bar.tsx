import React, { useState, useEffect } from 'react';
import {
  AppBar,
  Button,
  IconButton,
  Menu,
  MenuItem,
} from '@mui/material';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link, useLocation } from 'react-router-dom';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import SearchIcon from '@mui/icons-material/Search';
import MenuIcon from '@mui/icons-material/Menu';
import Box from '@mui/material/Box';

const buttonStyles = {
  background: 'inherit',
  color: '#FFFFFF',
  '&:hover': {
    color: '#880808',
  },
};

const selectedButtonStyles = {
  color: '#DE3163',
  fontWeight: 'bold',
  '&:hover': {
    color: '#880808',
  },
};

const Bar = () => {
  const location = useLocation();
  const [isSmallScreen, setIsSmallScreen] = useState(window.innerWidth < 600);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event:any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    const handleResize = () => {
      setIsSmallScreen(window.innerWidth < 600);
    };

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <Box sx={{ mb: 2, boxShadow: 3 }}>
      <AppBar position="static" sx={{ backgroundColor: '#000000' }}>
        <Toolbar>
          <img src=".././public/film2.png" alt="logo" width="60" height="40" />
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, color: '#E30B5C', fontWeight: 'bold' }}>
            Filmi
          </Typography>
          {isSmallScreen ? (
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={handleClick}
            >
              <MenuIcon />
            </IconButton>
          ) : (
            <>
              <Button
                component={Link}
                to="/pop"
                sx={location.pathname === '/pop' ? selectedButtonStyles : buttonStyles}
              >
                Trending <TrendingUpIcon />
              </Button>
              <Button
                component={Link}
                to="/moji_filmi"
                sx={location.pathname === '/moji_filmi' ? selectedButtonStyles : buttonStyles}
              >
                Moj seznam <FavoriteBorderIcon />
              </Button>
              <Button
                component={Link}
                to="/filter_filmi"
                sx={location.pathname === '/filter_filmi' ? selectedButtonStyles : buttonStyles}
              >
                Išči <SearchIcon />
              </Button>
            </>
          )}
          <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem
              component={Link}
              to="/pop"
              onClick={handleClose}
            >
              Trending <TrendingUpIcon />
            </MenuItem>
            <MenuItem
              component={Link}
              to="/moji_filmi"
              onClick={handleClose}
            >
              Moj seznam <FavoriteBorderIcon />
            </MenuItem>
            <MenuItem
              component={Link}
              to="/filter_filmi"
              onClick={handleClose}
            >
              Išči <SearchIcon />
            </MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Bar;