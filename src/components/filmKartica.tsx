import React, { useState, useEffect } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';

import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import TranslateIcon from '@mui/icons-material/Translate';
import DateRangeIcon from '@mui/icons-material/DateRange';
import { Divider } from '@mui/material';

export default function FilmKartica({ film, odstraniIzLocalStorage }: { film: any; odstraniIzLocalStorage: (filmId: any) => void }) {
  const [jePobarvano, setJePobarvano] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);

  const openDialog = () => {
    setDialogOpen(true);
  };

  const closeDialog = () => {
    setDialogOpen(false);
  };


  useEffect(() => {
    const mojiFilmiString = localStorage.getItem('mojiFilmi');
    const mojiFilmi = mojiFilmiString ? JSON.parse(mojiFilmiString) : [];

    const jeVSeznamu = mojiFilmi.some((mfilm: any) => mfilm.id === film.id);

    if (jeVSeznamu) {
      setJePobarvano(true);
    }
  }, [film.id]);

  const dodajVLocalStorage = (film: any) => {
    const mojiFilmiString = localStorage.getItem('mojiFilmi');
    const mojiFilmi = mojiFilmiString ? JSON.parse(mojiFilmiString) : [];

    const jeVSeznamu = mojiFilmi.some((mfilm: any) => mfilm.id === film.id);

    if (jeVSeznamu) {
      const noviMojiFilmi = mojiFilmi.filter((mfilm: any) => mfilm.id !== film.id);
      localStorage.setItem('mojiFilmi', JSON.stringify(noviMojiFilmi));
      setJePobarvano(false);
      odstraniIzLocalStorage(film.id);
    } else {
      mojiFilmi.push(film);
      localStorage.setItem('mojiFilmi', JSON.stringify(mojiFilmi));
      setJePobarvano(true);
    }
  };

  const karticaStyle = {

    display: 'flex',
    flexDirection: 'column',
    boxShadow: 3

  };

  const slikaStyle = {
    height: 0,
    paddingTop: '150%',
  };

  const modalContentStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '16px',
  };

  function getVoteColor(vote: number) {
    if (vote < 5) {
      return 'red';
    } else if (vote >= 5 && vote < 7) {
      return 'orange';
    } else {
      return 'green';
    }
  }

  return (
    <>
      <div className="film-kartica-container">
        <Card sx={karticaStyle}>
          <CardMedia
            sx={slikaStyle}
            image={`https://image.tmdb.org/t/p/w500/${film.poster_path}`}
            title={film.title}
            onClick={openDialog}
            style={{ position: 'relative' }}
          >
            {jePobarvano && (
              <FavoriteIcon
                style={{
                  color: 'red',
                  position: 'absolute',
                  top: '8px',
                  right: '8px', 
                  backgroundColor: 'black',
                  borderRadius: '100%',
                }}
                onClick={() => dodajVLocalStorage(film)}
              />
            )}
          </CardMedia>
        </Card>
      </div>

      <Dialog open={dialogOpen} onClose={closeDialog} PaperProps={{ sx: { backgroundColor: 'black' } }}>
        <DialogTitle align="center" sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', color: 'white' }}>
          {film.title}


        </DialogTitle>
        <Typography
          align="center"
          variant="body2"
          color="text.primary"
          sx={{

            fontWeight: 'bold',
            backgroundColor: getVoteColor(film.vote_average),
            color: 'white',
            padding: '4px 8px',
            borderRadius: '2%',
            marginLeft: '35%',
            marginRight: '35%',
          }}
        >
          {Math.round(film.vote_average)}/10
          <Typography
            variant="body2"
            color="text.primary"
            sx={{
              fontSize: "10px",
              color: "white",
              ml: 2

            }}
          >
            Število ocen:{film.vote_count}
          </Typography>
        </Typography>
        <Divider />
        <Divider />
        <DialogContent sx={{ color: 'white' }}>
          <DialogContentText sx={{ color: 'white' }}>{film.overview}</DialogContentText>
          <Divider sx={{ my: 2, color: 'white' }} />
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <DateRangeIcon sx={{ color: "#696969" }} />
              <Typography variant="body2" color="#696969">
                {film.release_date}
              </Typography>
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <TranslateIcon sx={{ color: "#696969" }} />
              <Typography variant="body2" color="#696969">
                {film.original_language}
              </Typography>
            </div>
            <CardActions>
              Moj list: {jePobarvano ? (
                <FavoriteIcon style={{ color: 'red' }} onClick={() => dodajVLocalStorage(film)} />
              ) : (
                <FavoriteBorderIcon
                  onClick={() => {
                    dodajVLocalStorage(film);
                    closeDialog();
                  }}
                  style={{ color: 'inherit' }}
                />
              )}
            </CardActions>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
}