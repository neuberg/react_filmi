import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import PopFilmi from './popFilmi';
import MojiFilmi from './mojiFilmi';
import FilterFilmi from './filterFilmi';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<PopFilmi />} />
        <Route path="/moji_filmi" element={<MojiFilmi />} />
        <Route path="/pop" element={<PopFilmi />} />
        <Route path="/filter_filmi" element={<FilterFilmi />} />
      </Routes>
    </Router>
  );
}

export default App;





