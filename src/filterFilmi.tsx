import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import { Box, Grid } from '@mui/material';
import Bar from './components/Bar';
import { filmApi } from './api/filmApi';
import FilmKartica from './components/filmKartica';

export default function FilterFilmi() {
  const [data, setData] = useState<any | null>(null);
  const [vnos, setVnos] = useState('');

  const iskanjePoVnosu = async () => {
    try {
      const responseData = await filmApi(
        `/search/movie?query=${vnos}&include_adult=false&language=en-US&page=1`
      );
      setData(responseData);
    } catch (error) {
      console.error(error);
    }
  };

  const ročnoIskanje = () => {
    iskanjePoVnosu();
  };

  console.log(data);

  return (
    <>
      <Grid
        style={{
          position: 'fixed',
          top: 0,
          width: '100%',
          height: '100%',
          paddingRight: 30,
          overflowY: 'auto',
        }}
      >
        <Bar />
        <Paper
          component="form"

          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            maxWidth: 800,
            mb: 2,
            marginLeft: 'auto',
            marginRight: 'auto',
            backgroundColor: "#DCDCDC"
          }}

        >
          <IconButton sx={{ p: '10px' }} aria-label="menu">
            <MenuIcon />
          </IconButton>
          <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder="Vnesite ključne besede"
            inputProps={{ 'aria-label': 'Vnesite ključne besede' }}
            value={vnos}
            onChange={(e) => setVnos(e.target.value)}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                e.preventDefault();
                ročnoIskanje();
              }
            }}
          />
          <IconButton
            type="button"
            sx={{ p: '10px' }}
            aria-label="search"
            onClick={ročnoIskanje}
          >
            <SearchIcon />
          </IconButton>
          <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
          <IconButton color="primary" sx={{ p: '10px' }} aria-label="directions" />
        </Paper>

        <Grid container spacing={2}>
          {data ? (
            data.results.map((film: any, index: any) => (
              <Grid item xs={12} sm={6} md={4} lg={2} key={index}>
                <FilmKartica film={film} odstraniIzLocalStorage={() => { }} />
              </Grid>
            ))
          ) : (
            <p>...</p>
          )}
        </Grid>
      </Grid>
    </>
  );
}