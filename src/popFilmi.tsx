import { useState, useEffect } from 'react';
import './css/App.css';
import { filmApi } from './api/filmApi';
import Grid from '@mui/material/Grid';
import FilmKartica from './components/filmKartica';
import Bar from './components/Bar';

export default function PopFilmi() {
    const [data, setData] = useState<any | null>(null);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const responseData = await filmApi('trending/movie/day?language=en-US');
                setData(responseData);
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, []);
    console.log(data);

    return (
        <>
            <Grid style={{ position: 'fixed', top: 0, width: '100%', height: '100%', paddingRight: 30, overflowY: 'auto' }}>
                <Bar />
                <Grid container spacing={2}>
                    {data ? (
                        data.results.map((film: any, index: any) => (
                            <Grid item xs={12} sm={6} md={4} lg={2} key={index}>
                                <FilmKartica film={film} odstraniIzLocalStorage={() => { }} />
                            </Grid>
                        ))
                    ) : (
                        <p>Nalagam...</p>
                    )}
                </Grid>
            </Grid>
        </>
    );
}


