# React: Iskanje filmov
## Tehnologije
- **TypeScript**
- **MUI**
- **Vite**
- **Axios**


## Zaslonski posnetki


![Main](filmi_screen/desk_1.png)

----

![Select](filmi_screen/Screenshot_1.1.png)

----


![Adding](filmi_screen/desk_2.png)

----


![Filter](filmi_screen/desk_3.png)

----


![Main - phone](filmi_screen/mobile_1.png)


----


![Adding - phone](filmi_screen/mobile_1.1.png)

----


![Filter - phone](filmi_screen/Screenshot_3.png)

----



## Namestitev

1. Kloniranje repozitorija

2. Namestitev /node_modules

    - Odpremo terminal (win + R in vtipkamo _cmd_ ter pritisnemo enter)

    - V terminal vpišemo ukaz

      ```
      npm install
      ```
    

    - Počakamo, da se namesti, vse kar je potrebno

3. Da poženemo strežnik, v terminal vpišemo ukaz
      
      ```
      npm run dev
      ```

4. Za dostop do strani v brskalnik prekopiramo URL 

      ```
      http://localhost:5173/
      ```
